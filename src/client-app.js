/*
-----------------------
Sockets configuration
-----------------------
*/


/* global state object */
var cs = {
    displayState: 'slider',
    playState: 'play'
};

class App {
    constructor(slider) {

        this.utils = new Utils(slider);
        this.$slider = slider;

    }

    initSlider(config) {
        this.$slider.bxSlider({
            adaptiveHeight: true,
            mode: 'fade',
            auto: true,
            pause: config.duration || 1000
        });
    }
    makeSlider() {
        cs.display = 'slider';
        $('.bx-controls').show();
        $('.bx-viewport').css('overflow', 'hidden');
        this.$slider.find('li').attr('style', '');
        this.$slider.removeClass('dashboard-view');

        this.utils.startSlider();
    }

    makeDashboard() {
        this.utils.stopSlider();

        cs.display = 'dashboard';

        this.$slider.addClass('dashboard-view');
        this.$slider.find('li').attr('style', '').show().css('height', $(window).height() / 3);

        $('.bx-viewport').css('overflow', 'visible');
        $('.bx-controls').hide();
    }

    clientActionsDispatcher(data) {
        if (data.type == 'update') {
            this.utils.updateSliderHTML(data);
        }
        if (data.type == 'statuschange') {
            this.utils.updateSliderStatus(data);
        }
        if (data.type == 'playpause') {
            cs.playState == 'play' ? this.utils.stopSlider() : this.utils.startSlider();
        }
        if (data.type == 'toggledisplay') {
            cs.displayState == 'dashboard' ? this.makeSlider() : this.makeDashboard();
        }
        if (data.type == 'togglefullscreen') {
            //todo
        }
        if (data.type == 'chatmsg') {
            this.utils.showChatMessage(data.data);
        }
    }


}

class Utils {
    constructor(s) {
        this.slider = s;
    }
    stopSlider() {
        this.slider.stopAuto();
        cs.playState == 'stop';
    }

    startSlider() {
        this.slider.startAuto();
        cs.playState == 'play';
    }

    updateSliderHTML(objData) {
        $('#slide' + objData.item + ' .slide-' + objData.partName).text(objData.newValue);
    }
    updateSliderStatus(objData) {
        $('#slide' + objData.item).removeAttr('class');
        $('#slide' + objData.item).addClass(objData.status);
    }
    showChatMessage(msgDetails) {
        $('.chat-message__author').html(msgDetails.author);
        $('.chat-message__content').html(msgDetails.content);
        $('.chat-message').addClass('active');
        setTimeout(() => {
            $('.chat-message').removeClass('active');
        }, 2500);
    }

    // $('#enable-fullscreen').click(function(event) {
    //     toggleFullScreen('slider');
    // });
}




class Socket {
    constructor(appReference) {

        this.parent = appReference;
        this.socket = io.connect('http://localhost:5000');

        this.initSocketsEvents();
    }
    initSocketsEvents() {
        this.socket.on('servermessage', (data) => {
            if (data.type == 'sliderinit') {
                this.parent.initSlider(data.data);
            }
        }).on('msgforclient', (data) => this.parent.clientActionsDispatcher(data));
    }
}


$(document).ready(function() {

    var app = new App($('.bxslider'));
    var s = new Socket(app);

});
