var sc = {
    changed: false
};

class AdminApp {
    constructor() {

        this.sock = new Sockets();
    }

    initUI() {

        $('#send-slide-change').click(() => {

            var data = {
                type: 'update',
                item: $('#slide-id').find('option:selected').val(),
                partName: 'title',
                newValue: $('#msg').val()
            };
            this.sock.emitChange(data);

        });

        $('#send-status-toggle').click(() => {
            $(this).toggleClass('active');
            //parts of the slide: body > (title + content)|(image)
            var data = {
                type: 'statuschange',
                item: $('#slide-id').find('option:selected').val(),
                status: ($(this).hasClass('active') ? 'active' : 'fail')
            };
            this.sock.emitChange(data);

        });


        $('#toggle-view-change').click((e) => {

            $(e.target).prev().toggleClass('glyphicon-film glyphicon-th-large');
            this.sock.emitChange({
                type: 'toggledisplay'
            });
        });

        $('#play-pause-change').click((e) => {
            $(e.target).prev().toggleClass('glyphicon-play glyphicon-pause');
            this.sock.emitChange({
                type: 'playpause'
            });
        });

        $('#send-chat-message').click((e) => {
            var text = $(e.target).prev().val();
            this.sock.emitChange({
                type: 'chatmsg',
                data: {
                    content: text,
                    author: 'admin'
                }
            });
            $(e.target).prev().val('');
        });


        $('.slide-block form input').change((event) => {
            sc.changed = true;
            $(this).parents('.slide-block').find('.button-block button[data-type="slide-change"]').show();
        });

    }
}

class Sockets {
    constructor() {
        this.socket = io.connect('http://localhost:5000');

        this.socket.on('servermessage', function(data) {
            console.log('message -> ', data.message);
        });
    }
    emitChange(dataObject) {
        this.socket.emit('msgfromadmin', dataObject);
    }
}


$(document).ready(function() {
    var app = new AdminApp();
    app.initUI();
});
