var express = require('express');
//var cheerio = require('cheerio');
//var request = require('request');
var url = require('url');
var router = express.Router();


module.exports = function(app) {

    return {
        renderPanel: function(req, res) {

            res.setHeader('Content-Type', 'text/html');

            res.render('./pages/slider-admin.ejs', {
                jsonData: app.configs.sliderData.slides,
                general: app.appConfig.general,
                settingsAdmin: app.appConfig.admin
            });
        }
    }
}
