var express = require('express');
//var cheerio = require('cheerio');
//var request = require('request');
var url = require('url');
var router = express.Router();


module.exports = function(app) {

    return {
        renderSlider: (req, res) => {

            res.setHeader('Cache-Control', 'no-cache');
            res.setHeader('Content-Type', 'text/html');

            res.render('./pages/slider.ejs', {
                slidesData: app.configs.sliderData.slides,
                general: app.appConfig.general,
                settingsClient: app.appConfig.client
            });
        }
    }
}
