import React from 'react';
//import Slide from './Slide'
//import SliderHeader from './SliderHeader'
/*
that's how it should look like !
http://codepen.io/JonasB/pen/PwWXqg
*/

class SlideList extends React.Component {
    render() {
        const slide = this.props.data.map((obj) => {
            return <Slide title={obj.title} type={obj.type} other={obj}></Slide>
        });
        return (
            <div className="slideList">
                {slide}
            </div>
        );
    }
}

class Slide extends React.Component {
    constructor() {
        super()
        this.render = this.render.bind(this);
        this.state = {duration: 0};
        this.handleClick = this.handleClick.bind(this);
    }
    // shouldComponentUpdate() {
    //   return this.state.duration !== this.props.id;
    // }
    handleClick() {
        //this.state.duration = this.state.duration + 1;
        this.setState({duration: this.state.duration + 1});
    }
    render() {
        const title = this.props.title;
        const details = this.props.type;
        const duration = this.state.duration;
        return (
            <div style={slideStyle}><a onClick={this.handleClick}>{title}</a> / {details} / {duration}</div>
            );
    }
}

Slide.propTypes = {
    title:   React.PropTypes.string.isRequired,
    type:     React.PropTypes.string.isRequired,
    duration:     React.PropTypes.number
};

let slideStyle = {
    border: "1px solid black"
};

class SliderHeader extends React.Component {

    toggleFullScreen() {
      var elem = document.getElementsByTagName('main')[0];
        if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
            if (elem.requestFullScreen) {
                elem.requestFullScreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullScreen) {
                elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            } else if (elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }
    }

    render() {
        const title = this.props.children;
        return (
            <header>
              <h4>{title}</h4>
              <button onClick={this.toggleFullScreen}>fullscreen</button>
            </header>
        );
    }
}


export default class SliderBox extends React.Component {
  render() {
    return (
        <main style={mainStyle}>
          <SliderHeader>{this.props.data.title}</SliderHeader>
          <SlideList data={this.props.data.slides}></SlideList>
        </main>
        );
  }
}

let mainStyle = {
    width: "100%",
    height: "100%"
};
