import React from 'react';

export default class Slide extends React.Component {
    constructor() {
        super()
        this.render = this.render.bind(this);
        this.state = {duration: 0};
    }
    handleSetState() {
        this.setState({duration: this.state.duration + 1});
        console.log(this.state);
    }
    render() {
        const title = this.props.title;
        const details = this.props.type;
        const duration = this.state.duration;
        return (
            <div style={slideStyle}><a onClick={this.handleSetState}>{title}</a> / {details} / {duration}</div>
            );
    }
}

Slide.propTypes = {
    title:   React.PropTypes.string.isRequired,
    type:     React.PropTypes.string.isRequired,
    duration:     React.PropTypes.number
};

let slideStyle = {
    border: "1px solid black"
};