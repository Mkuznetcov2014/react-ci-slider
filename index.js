var React = require('react');
var ReactDOMServer = require('react-dom/server');
var DOM = React.DOM;
var body = DOM.body;
var div = DOM.div;
var script = DOM.script;

var browserify = require('browserify');
var babelify = require("babelify");
//var cheerio = require('cheerio');
//var request = require('request');
var glob = require('glob');
var url = require('url');
//var async = require('async');
//var path = require('path');

/* setup event system */
const EventEmitter = require('events');
const myEmitter = new EventEmitter();
/* setup event system */


/* load express */
var express = require('express');
var app = express();
app.configs = {}

require('babel/register')({
    ignore: false
});

/* load configs */
var sliderData = {};
app.appConfig = require('./config/app.json');
var db = require('./config/db.json');


/*
 * init mongodb 
 */
const MongoClient = require('mongodb').MongoClient
const connString = 'mongodb://' + db.user + ':' + db.password + '@' + db.dbstring;

MongoClient.connect(connString, (err, database) => {

    if (err) return console.log(err)
    db = database

    db.collection('slider').findOne(function(err, results) {
        if (err) return console.log(err)
        app.configs.sliderData = results;
    })


    /*
    db.collection('quotes').save({
        'foo': 42,
        'bar': 'baz'
    }, (err, result) => {
        if (err) return console.log(err)

        console.log('saved to database');
    })
*/
})


var SliderBox = require('./app/views/index.jsx');



app.set('port', (process.argv[2] || 3000));
app.set('view engine', 'jsx');
app.set('views', __dirname + '/app/views');
app.engine('jsx', require('express-react-views').createEngine({
    transformViews: false
}));

app.disable('view cache');

app.set('appPath', 'public');
var ss = express.static(__dirname + '/public');
app.use(ss);

// var controllers = glob.sync(__dirname + '/app/controllers/*.js');
// controllers.forEach(function(controller) {
//     require(controller);
// });
var admin = require(__dirname + '/app/controllers/admin.js')(app);
var client = require(__dirname + '/app/controllers/client.js')(app);

app.use('/slider', client.renderSlider);
app.use('/admin', admin.renderPanel);

app.use('/chat', function(req, res) {

    var url_parts = url.parse(req.url, true);
    var queryText = url_parts.query.text;
    var chatMsgData = {
        type: 'chatmsg',
        data: {
            content: queryText,
            author: 'chat API'
        }
    };

    myEmitter.emit('sendajaxthroughsocket', chatMsgData);

    res.setHeader('Content-Type', 'text/html');
    res.write('<p>Chat msg recieved</p>' + queryText);
    res.end();
});




/* sockets part */
var server = require('http').Server(app);
var io = require('socket.io')(server);
var listener = io.listen(5000);

io.on('connection', function(socket) {

    console.log('socket connection works..');

    myEmitter.on('sendajaxthroughsocket', function(data) {
        socket.broadcast.emit('msgforclient', data);
    });

    /*
     * inital sockets call
     * when do client side renderingh of slides - provide data for templating
     * when slides are rendered on server - acts as a trigger
     */
    socket.emit('servermessage', {
        type: 'sliderinit',
        data: app.configs.sliderData.defaults
    });

    socket.on('msgfromadmin', (data) => {
        socket.broadcast.emit('msgforclient', data);
    });

});

/* end : sockets part */

/* 
 * 3rd party pages call


sample usage via emitters
--
var emitter = new EventEmitter
emitter.on(‘triggered’, function () {
console.log(‘The event was triggered’)
})
emitter.emit(‘triggered’)
--

function delay (n) {
return new Promise(function (resolve, reject) {
setTimeout(function () {
resolve(n)
}, n)
})
}
var promise = delay(100)
.then(function (n) {
return delay(n)
})
.then(function (n) {
console.
 */

app.get('/log/goal', (req, res) => {
    const options = {
        host: 'http://localhost',
        path: '/dev/#/documentbooking/book/9869',
        port: 9001,
        method: 'GET'
    };

    //todo: add authorisation - https://www.npmjs.com/package/request

    var requestline = options.host + ':' + options.port + options.path;

    request(requestline, (error, response, body) => {
        if (!error && response.statusCode == 200) {
            console.log(body) // Show the HTML for the Google homepage. 
        }
        res.end();
    })

});





/* 
 * App startup
 */

app.listen(app.get('port'), () => {
    console.log('App running on ', app.get('port'));
});



/*
app.use('/bundle.js', function(req, res) {
    res.setHeader('content-type', 'application/javascript');

    browserify({
            debug: true
        })
        .transform(babelify.configure({
            presets: ["react", "es2015"]
        }))
        .require("./app.js", {
            entry: true
        })
        .bundle()
        .pipe(res);
});
*/
/*
app.use('/', function(req, res) {

    // var html = ReactDOMServer.renderToStaticMarkup(body(null,
    //     div({
    //         id: 'app',
    //         dangerouslySetInnerHTML: {
    //             __html: markup
    //         }
    //     }),
    //     script({
    //         id: 'initial-data',
    //         type: 'text/plain',
    //         'data-json': initialData
    //     }),
    //     script({
    //         src: '/build/bundle.js'
    //     })
    // ));
    // res.end(html);

    res.setHeader('Content-Type', 'text/html');
    var initialData = JSON.stringify(data);
    var markup = ReactDOMServer.renderToString(React.createElement(SliderBox, {
        data: data
    }));
    //  var reactHtml = React.renderToString(ReactApp({}));
    res.render('main.ejs', {
        reactOutput: markup,
        jsonData: initialData
    });
});
*/

// for static rendering
//
// app.use('/', function(req, res) {
//     res.render('index', {
//         data: data
//     });
// });
