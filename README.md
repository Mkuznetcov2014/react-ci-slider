
# USAGE
## To start
nodemon [--harmony] index.js [port]

## Urls
 - Open localhost:port/admin to make edits in configs
 - Open localhost:port/slider to see slider

## Configs
 - see /config for app and slides configs



# TODOS
## 1. ui
 - http://www.bootstrapzero.com/blocks/
 - http://www.blacktie.co/demo/dashgum/index.html

## 2.client code
 - use RxJS 
 + use ES6 transpiler
 - add reordering of items
 - add refresh slide button in admin

## 3.general/backend
 - add secure toket for admin
 + add /chat API pont 
 - add persisting in mongodb
 - add authorisation


 ## Howtos
 - mongodb : http://www.zell-weekeat.com/crud-express-mongodb/?utm_source=nodeweekly&utm_medium=email 