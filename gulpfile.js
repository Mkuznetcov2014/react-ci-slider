var gulp = require('gulp');
var rimraf = require('gulp-rimraf');
var concat = require('gulp-concat');
var watch = require("gulp-watch");
var browserify = require('browserify');
var watchify = require('watchify');
var babelify = require("babelify");
const babel = require('gulp-babel');

const APP_PUBLIC_FOLDER = 'public/assets/';
const APP_SCRIPTS = APP_PUBLIC_FOLDER + 'js/dist/';
const APP_CLIENTSIDE_SRC = 'src/*.js';
/*
------------------------
CONFIGURE THIS ABSOLUTELY
------------------------
*/



gulp.task('babel', function() {
    return gulp.src(APP_CLIENTSIDE_SRC)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(APP_SCRIPTS));
});
/*
------------------------
    ^^^^^^^^^^^^^^^^
------------------------
*/
/*
gulp.task('browserify', function() {

    browserify({
            debug: true
        })
        .transform(babelify.configure({
            presets: ["react", "es2015"]
        }))
        .require("./app.js", {
            entry: true
        })
        .bundle()
        .pipe(gulp.dest('./public/'));

    // var bundler = browserify({
    //     entries: ['./views/index.jsx'], // Only need initial file, browserify finds the deps
    //     transform: babelify.configure({
    //         presets: ["react", "es2015"]
    //     }), // We want to convert JSX to normal javascript
    //     debug: true, // Gives us sourcemapping
    //     cache: {}, packageCache: {}, fullPaths: true // Requirement of watchify
    // });
    // var watcher  = watchify(bundler);
    //
    // return watcher
    // .on('update', function () { // When any files update
    //     var updateStart = Date.now();
    //     console.log('Updating!');
    //     watcher.bundle() // Create new bundle that uses the cache for high performance
    //     .pipe(source('bundle.js'))
    // // This is where you add uglifying etc.
    //     .pipe(gulp.dest('./public/'));
    //     console.log('Updated!', (Date.now() - updateStart) + 'ms');
    // })
    // .bundle() // Create the initial bundle when starting the task
    // .pipe(source('bundle.js'))
    // .pipe(gulp.dest('./public/'));
});

*/


gulp.task('css', function() {
    gulp.watch('less/**/*.css', function() {
        return gulp.src('less/**/*.css')
            .pipe(concat('style.css'))
            .pipe(gulp.dest('public/assets/'));
    });
});

gulp.task('clearJS', function() {
    return gulp.src(APP_SCRIPTS + '/*', {
            read: false
        }) // much faster
        .pipe(rimraf());
});

gulp.task('watchjs', function() {
    return watch(APP_CLIENTSIDE_SRC, function(obj) {

        console.log('watch event - ', obj.base);

        return gulp.src(APP_CLIENTSIDE_SRC)
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(APP_SCRIPTS));
    })
});

gulp.task('default', ['clearJS', 'babel']);
