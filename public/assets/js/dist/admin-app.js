'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var sc = {
    changed: false
};

var AdminApp = function () {
    function AdminApp() {
        _classCallCheck(this, AdminApp);

        this.sock = new Sockets();
    }

    _createClass(AdminApp, [{
        key: 'initUI',
        value: function initUI() {
            var _this = this;

            $('#send-slide-change').click(function () {

                var data = {
                    type: 'update',
                    item: $('#slide-id').find('option:selected').val(),
                    partName: 'title',
                    newValue: $('#msg').val()
                };
                _this.sock.emitChange(data);
            });

            $('#send-status-toggle').click(function () {
                $(_this).toggleClass('active');
                //parts of the slide: body > (title + content)|(image)
                var data = {
                    type: 'statuschange',
                    item: $('#slide-id').find('option:selected').val(),
                    status: $(_this).hasClass('active') ? 'active' : 'fail'
                };
                _this.sock.emitChange(data);
            });

            $('#toggle-view-change').click(function (e) {

                $(e.target).prev().toggleClass('glyphicon-film glyphicon-th-large');
                _this.sock.emitChange({
                    type: 'toggledisplay'
                });
            });

            $('#play-pause-change').click(function (e) {
                $(e.target).prev().toggleClass('glyphicon-play glyphicon-pause');
                _this.sock.emitChange({
                    type: 'playpause'
                });
            });

            $('#send-chat-message').click(function (e) {
                var text = $(e.target).prev().val();
                _this.sock.emitChange({
                    type: 'chatmsg',
                    data: {
                        content: text,
                        author: 'admin'
                    }
                });
                $(e.target).prev().val('');
            });

            $('.slide-block form input').change(function (event) {
                sc.changed = true;
                $(_this).parents('.slide-block').find('.button-block button[data-type="slide-change"]').show();
            });
        }
    }]);

    return AdminApp;
}();

var Sockets = function () {
    function Sockets() {
        _classCallCheck(this, Sockets);

        this.socket = io.connect('http://localhost:5000');

        this.socket.on('servermessage', function (data) {
            console.log('message -> ', data.message);
        });
    }

    _createClass(Sockets, [{
        key: 'emitChange',
        value: function emitChange(dataObject) {
            this.socket.emit('msgfromadmin', dataObject);
        }
    }]);

    return Sockets;
}();

$(document).ready(function () {
    var app = new AdminApp();
    app.initUI();
});