'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*
-----------------------
Sockets configuration
-----------------------
*/

/* global state object */
var cs = {
    displayState: 'slider',
    playState: 'play'
};

var App = function () {
    function App(slider) {
        _classCallCheck(this, App);

        this.utils = new Utils(slider);
        this.$slider = slider;
    }

    _createClass(App, [{
        key: 'initSlider',
        value: function initSlider(config) {
            this.$slider.bxSlider({
                adaptiveHeight: true,
                mode: 'fade',
                auto: true,
                pause: config.duration || 1000
            });
        }
    }, {
        key: 'makeSlider',
        value: function makeSlider() {
            cs.display = 'slider';
            $('.bx-controls').show();
            $('.bx-viewport').css('overflow', 'hidden');
            this.$slider.find('li').attr('style', '');
            this.$slider.removeClass('dashboard-view');

            this.utils.startSlider();
        }
    }, {
        key: 'makeDashboard',
        value: function makeDashboard() {
            this.utils.stopSlider();

            cs.display = 'dashboard';

            this.$slider.addClass('dashboard-view');
            this.$slider.find('li').attr('style', '').show().css('height', $(window).height() / 3);

            $('.bx-viewport').css('overflow', 'visible');
            $('.bx-controls').hide();
        }
    }, {
        key: 'clientActionsDispatcher',
        value: function clientActionsDispatcher(data) {
            if (data.type == 'update') {
                this.utils.updateSliderHTML(data);
            }
            if (data.type == 'statuschange') {
                this.utils.updateSliderStatus(data);
            }
            if (data.type == 'playpause') {
                cs.playState == 'play' ? this.utils.stopSlider() : this.utils.startSlider();
            }
            if (data.type == 'toggledisplay') {
                cs.displayState == 'dashboard' ? this.makeSlider() : this.makeDashboard();
            }
            if (data.type == 'togglefullscreen') {
                //todo
            }
            if (data.type == 'chatmsg') {
                this.utils.showChatMessage(data.data);
            }
        }
    }]);

    return App;
}();

var Utils = function () {
    function Utils(s) {
        _classCallCheck(this, Utils);

        this.slider = s;
    }

    _createClass(Utils, [{
        key: 'stopSlider',
        value: function stopSlider() {
            this.slider.stopAuto();
            cs.playState == 'stop';
        }
    }, {
        key: 'startSlider',
        value: function startSlider() {
            this.slider.startAuto();
            cs.playState == 'play';
        }
    }, {
        key: 'updateSliderHTML',
        value: function updateSliderHTML(objData) {
            $('#slide' + objData.item + ' .slide-' + objData.partName).text(objData.newValue);
        }
    }, {
        key: 'updateSliderStatus',
        value: function updateSliderStatus(objData) {
            $('#slide' + objData.item).removeAttr('class');
            $('#slide' + objData.item).addClass(objData.status);
        }
    }, {
        key: 'showChatMessage',
        value: function showChatMessage(msgDetails) {
            $('.chat-message__author').html(msgDetails.author);
            $('.chat-message__content').html(msgDetails.content);
            $('.chat-message').addClass('active');
            setTimeout(function () {
                $('.chat-message').removeClass('active');
            }, 2500);
        }

        // $('#enable-fullscreen').click(function(event) {
        //     toggleFullScreen('slider');
        // });

    }]);

    return Utils;
}();

var Socket = function () {
    function Socket(appReference) {
        _classCallCheck(this, Socket);

        this.parent = appReference;
        this.socket = io.connect('http://localhost:5000');

        this.initSocketsEvents();
    }

    _createClass(Socket, [{
        key: 'initSocketsEvents',
        value: function initSocketsEvents() {
            var _this = this;

            this.socket.on('servermessage', function (data) {
                if (data.type == 'sliderinit') {
                    _this.parent.initSlider(data.data);
                }
            }).on('msgforclient', function (data) {
                return _this.parent.clientActionsDispatcher(data);
            });
        }
    }]);

    return Socket;
}();

$(document).ready(function () {

    var app = new App($('.bxslider'));
    var s = new Socket(app);
});